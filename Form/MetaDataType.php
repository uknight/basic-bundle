<?php

namespace Uknight\BasicBundle\Form;

use Symfony\Component\Form\AbstractType;
use Uknight\BasicBundle\Entity\MetaData;
use Symfony\Component\Form\FormBuilderInterface;
use Uknight\BasicBundle\Form\MetaDataTranslationType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class MetaDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', CollectionType::class, [
                'entry_type' => MetaDataTranslationType::class,
                'entry_options' => [
                    'label' => false
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MetaData::class,
        ]);
    }
}
