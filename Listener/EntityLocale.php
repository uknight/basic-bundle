<?php
/**
 * Created by PhpStorm.
 * User: flash
 * Date: 23.05.18
 * Time: 10:53
 */

namespace Uknight\BasicBundle\Listener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Translation\Translator;

class EntityLocale
{
    private $t;
    private $locales;

    public function __construct(Translator $translator, $locales) {
        $this->t = $translator;
        $this->locales = $locales;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if(method_exists($entity, 'setEntityLocale')) {
            $entity->setEntityLocale($this->t->getLocale(), $this->locales);
        }
    }

    public function onEasyadminPostnew(GenericEvent $event)
    {
        $entity = $event->getArgument('entity');//$event->getSubject();
//
////        print_r($entity); die;
        if(method_exists($entity, 'setEntityLocale')) {
            $entity->setEntityLocale($this->t->getLocale(), $this->locales);
////            print_r($entity); die;
        }
//
        $event['entity'] =  $entity;
////        print_r($event->getSubject()); die;
    }
//
//    public function onEasyadminPrenew(GenericEvent $event)
//    {
//        $entity = $event->getSubject();
//
////        print_r($entity); die;
//        if(method_exists($entity, 'setEntityLocale')) {
//            $entity->setEntityLocale($this->t->getLocale(), $this->locales);
//        }
//
//        $event['entity'] = $entity;
//        return $event;
//    }
}