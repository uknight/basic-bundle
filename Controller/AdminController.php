<?php

namespace Uknight\BasicBundle\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use Symfony\Component\HttpFoundation\Request;
use Uknight\BasicBundle\Entity\MetaData;


class AdminController extends BaseAdminController
{
    protected function initialize(Request $request)
    {
        $this->get('translator')->setLocale('en');
        parent::initialize($request);
    }

    protected function createNewUserEntity()
    {
        return $this->get('fos_user.user_manager')->createUser();
    }

    protected function prePersistUserEntity($user)
    {
        $this->get('fos_user.user_manager')->updateUser($user, false);
    }

    protected function createNewEntity()
    {
        $entityFullyQualifiedClassName = $this->entity['class'];

        $entity = new $entityFullyQualifiedClassName();
        $locales = $this->getParameter('app.locales');

        if(method_exists($entity, 'setEntityLocale')) {
            $entity->setEntityLocale($this->get('translator')->getLocale(), $locales);
        }

        return $entity;
    }

    protected function metaDataTranslation()
    {
        $entityFullyQualifiedClassName = $this->entity['class'];
        $entity = new $entityFullyQualifiedClassName();
        $meta = new MetaData();

        // set locales
        $locales = $this->getParameter('app.locales');

        if(method_exists($entity, 'setEntityLocale')) {
            $entity->setEntityLocale($this->get('translator')->getLocale(), $locales);
        }
        $meta->setEntityLocale($this->get('translator')->getLocale(), $locales);

        $entity->setMeta($meta);

        return $entity;
    }

    protected function createNewPageEntity()
    {
        return $this->metaDataTranslation();
    }
}
