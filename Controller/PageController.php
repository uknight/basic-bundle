<?php

namespace Uknight\BasicBundle\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Uknight\BasicBundle\Entity\Page;

class PageController extends Controller
{
    /**
     * @param null $slug
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{slug}", name="page")
     */
    public function index($slug = null)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var Page $page */
        $page = $em
            ->getRepository('UknightBasicBundle:Page')
            ->findOneBy([
                'slug' => $slug !== null
                    ? $slug
                    : $this->getParameter('homepage')
            ]);

        if ($page === null) {
            throw new NotFoundHttpException('Page was not found');
        }

        if ($page->getTemplate() == null) {
            return $this->render('pages/index.html.twig', [
                'page' => $page,
            ]);
        } else {
            return $this->render('pages/' . $page->getTemplate() . '.html.twig', [
                'page' => $page,
            ]);
        }
    }
}
