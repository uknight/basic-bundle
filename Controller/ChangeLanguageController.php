<?php

namespace Uknight\BasicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ChangeLanguageController extends Controller
{
    /**
     * Need this to change languages
     *
     * @param string  $locale  Language code for a language to use
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("lang/{locale}", name = "change_language")
     */
    public function langAction(Request $request, $locale)
    {
        if (array_search($locale, $this->getParameter('app.locales'), true) === false) {
            throw new NotFoundHttpException('No locale you\'ve searched');
        }

        $this->get('session')->set('_locale', $locale);
        if(
            $request->headers->get('referer') !== null
            && strpos($request->headers->get('referer'), $request->getSchemeAndHttpHost()) !== false
        ) {
            return $this->redirect($request->headers->get('referer'));
        } else {
            return $this->redirectToRoute('page');
        }
    }
}
