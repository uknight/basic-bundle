<?php

namespace Uknight\BasicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Uknight\BasicBundle\Entity\Parts\Timestamps;

/**
 * @ORM\Entity(repositoryClass="Uknight\BasicBundle\Repository\PageRepository")
 */
class Page
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=true)
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $template;

    /**
     * @ORM\OneToOne(targetEntity="Uknight\BasicBundle\Entity\MetaData", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $meta;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplate(string $template): self
    {
        $this->template = $template;

        return $this;
    }

    public function getMeta(): ?MetaData
    {
        return $this->meta;
    }

    public function setMeta(MetaData $meta): self
    {
        $this->meta = $meta;

        return $this;
    }
}
