<?php

namespace Uknight\BasicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Translation
 *
 * Base for translations entities for translating the entities
 *
 * @ORM\MappedSuperclass()
 */
abstract class Translation
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lang", type="string", length=2)
     */
    private $lang;

    /**
     * You need to implement many_to_one relation in order to use translations
     */
    protected $entity;


    /**
     * Get id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set lang
     *
     * @param string $lang
     *
     * @return $this
     */
    public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set entity
     *
     * @param string $entity
     *
     * @return $this
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Abstract toArray method
     *
     * You must implement it in your entity like this
     * (just return assoc array with the fields of
     * an entity that are translated):
     *
     *     public function toArray()
     *     {
     *         return [
     *             'firstname' => $this->getFirstName(),
     *             'lastname'  => $this->getLastName(),
     *             'position'  => $this->getPosition(),
     *             'adjective' => $this->getAdjective(),
     *             'title'     => $this->getTitle(),
     *         ];
     *     }
     */
    abstract function toArray();
}

