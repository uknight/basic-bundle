<?php

namespace Uknight\BasicBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * MetaData
 *
 * Used for pages metadata.
 * Have translations
 *
 * @ORM\Entity(repositoryClass="Uknight\BasicBundle\Repository\MetaDataRepository")
 */
class MetaData extends Translatable
{
    /**
     * @ORM\OneToMany(
     *     targetEntity="Uknight\BasicBundle\Entity\MetaDataTranslations",
     *     mappedBy="entity",
     *     orphanRemoval=true,
     *     cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    public function getTranslationClass()
    {
        return MetaDataTranslations::class;
    }
}
