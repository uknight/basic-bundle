<?php

namespace Uknight\BasicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Uknight\BasicBundle\Entity\Parts\Image;

/**
 * @ORM\Entity(repositoryClass="Uknight\BasicBundle\Repository\MetaDataTranslationsRepository")
 * @Vich\Uploadable()
 */
class MetaDataTranslations extends Translation
{
    use Image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $keywords;

    /**
     * @ORM\ManyToOne(targetEntity="Uknight\BasicBundle\Entity\MetaData", inversedBy="translations")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $entity;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function setKeywords(?string $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function toArray()
    {
        return [
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'keywords' => $this->getKeywords(),
            'image_img_meta' => $this->getImage(),
        ];
    }
}
