<?php

namespace Uknight\BasicBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Translatable
 *
 * Used for localisation purposes
 *
 * @ORM\MappedSuperclass()
 */
abstract class Translatable
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Internally used locale for translation
     *
     * @var string
     */
    private $entityLocale;

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|MetaDataTranslations[]
     */
    public function getTranslations(): Collection
    {
        return $this->translations;
    }

    public function addTranslation($translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setEntity($this);
        }

        return $this;
    }

    public function removeTranslation($translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getEntity() === $this) {
                $translation->setEntity(null);
            }
        }

        return $this;
    }

    abstract function getTranslationClass();

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function setEntityLocale($locale, $locales)
    {
        $this->entityLocale = $locale;
        $translationClass = $this->getTranslationClass();

        if ($this->translations->count() < count($locales)) {
            foreach ($locales as $locale) {
                foreach ($this->translations as $trans) {
                    if ($trans->getLang() == $locale) {
                        continue 2;
                    }
                }
                $translation = new $translationClass;
                $translation
                    ->setLang($locale)
                    ->setEntity($this)
                ;
                $this->translations->add($translation);
            }
        }
    }

    public function __get($name)
    {
        foreach ($this->getTranslations() as $translation) {
            if ($translation->getLang() == $this->entityLocale) {
                return $translation->{'get' . ucfirst($name)}();
            }
        }

        return null;
    }

    public function __call($name, $smth)
    {
        foreach ($this->getTranslations() as $translation) {
            if ($translation->getLang() == $this->entityLocale) {
                return $translation->{'get' . ucfirst($name)}();
            }
        }

        return null;
    }
}

