<?php

namespace Uknight\BasicBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Uknight\BasicBundle\Entity\MetaData;
use Uknight\BasicBundle\Entity\MetaDataTranslations;
use Uknight\BasicBundle\Entity\Page;

class SomePages extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $en = new MetaDataTranslations();
        $en
            ->setLang('en')
            ->setTitle('Homepage')
            ->setDescription('Home page of this site')
            ->setKeywords('home, page, homepage')
        ;

        $meta = new MetaData();
        $meta->addTranslation($en);

        $page = new Page();
        $date = new \DateTime();
        $page
            ->setName('Homepage')
            ->setSlug('homepage')
            ->setTemplate('homepage')
            ->setMeta($meta)
            ->setCreatedAt($date)
            ->setUpdatedAt($date)
        ;

        $manager->persist($en);
        $manager->persist($meta);
        $manager->persist($page);
        $manager->flush();
    }
}
