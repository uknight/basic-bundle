# U-Knight Basic Bundle for symfony 4

This bundle contains base entities and some other stuff to start site building of.

## Instruction

### Prerequisites

You need:

1. symfony
2. brain and straight arms
3. this installation instructions

### Installation

Add to your ```composer.json``` file following lines:

```json
    ...
    "repositories": [
        {
            "url": "git@bitbucket.org:uknight/basic-bundle.git",
            "type": "git"
        }
    ]
    ...
```

```json
    "require": {
        ...
        "uknight/basic-bundle": "@dev"
    },
```

Then run

```bash
$ composer install
```

### Configuration

Add following lines to your ```config/bundles.php``` file:

```php
return [
    ...
    Uknight\BasicBundle\UknightBasicBundle::class => ['all' => true],
];
```

You need to configure many things I'l write here later

###### TODO: write what to configure


## Usage

###### TODO: write how to use


## Credits

Big thanks to myself, Flash from U-Knight Web Studio LLC [u-knight.de](https://u-knight.de/en/) , for making this bundle ;)